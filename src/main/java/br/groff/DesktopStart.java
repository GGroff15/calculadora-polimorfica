package br.groff;

import br.groff.desktop.CalculadoraView;

public class DesktopStart {

	/**
	 * Launch the application.
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			final var window = new CalculadoraView();
			window.open();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

}
