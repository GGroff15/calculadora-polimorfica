package br.groff.operacao;

class OperacaoSubtracao implements IOperacao {

	@Override
	public int calcular(Valores valores) {
		return valores.subtrair();
	}

}
