package br.groff.operacao;

public final class OperacaoFactory {

	private OperacaoFactory() {
	}

	public static IOperacao criar(int idOperacao) {
		return switch (idOperacao) {
		case IOperacao.ADICAO -> new OperacaoAdicao();
		case IOperacao.SUBTRACAO -> new OperacaoSubtracao();
		case IOperacao.DIVISAO -> new OperacaoDivisao();
		case IOperacao.MULTIPLICACAO -> new OperacaoMultiplicacao();
		case IOperacao.POTENCIACAO -> new OperacaoPotenciacao();
		default -> throw new IllegalArgumentException("Unexpected value: " + idOperacao);
		};
	}

}
