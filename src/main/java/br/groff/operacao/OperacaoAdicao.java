package br.groff.operacao;

class OperacaoAdicao implements IOperacao {

	@Override
	public int calcular(Valores valores) {
		return valores.somar();
	}

}
