package br.groff.operacao;

final class OperacaoPotenciacao implements IOperacao {

	@Override
	public int calcular(Valores valores) {
		return valores.potenciar();
	}

}
