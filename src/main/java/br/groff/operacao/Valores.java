package br.groff.operacao;

public class Valores {

	private final int primeiroNumero;
	private final int segundoNumero;

	/**
	 * Construtor padrao, inicializa os campos com valores vazios nao nulos
	 *
	 * @param primeiroNumero
	 * @param segundoNumero
	 */
	public Valores(int primeiroNumero, int segundoNumero) {
		this.primeiroNumero = primeiroNumero;
		this.segundoNumero = segundoNumero;
	}

	int somar() {
		return primeiroNumero + segundoNumero;
	}

	int subtrair() {
		return primeiroNumero - segundoNumero;
	}

	int dividir() {
		return primeiroNumero / segundoNumero;
	}

	public int multiplicar() {
		return primeiroNumero * segundoNumero;
	}

	public int potenciar() {
		return (int) Math.pow(primeiroNumero, segundoNumero);
	}

}
