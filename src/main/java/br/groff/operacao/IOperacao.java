package br.groff.operacao;

public interface IOperacao {

	int ADICAO = 1;
	int SUBTRACAO = 2;
	int DIVISAO = 3;
	int MULTIPLICACAO = 4;
	int POTENCIACAO = 5;

	int calcular(Valores valores);

}
