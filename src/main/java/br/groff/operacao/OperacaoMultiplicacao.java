package br.groff.operacao;

class OperacaoMultiplicacao implements IOperacao {

	@Override
	public int calcular(Valores valores) {
		return valores.multiplicar();
	}

}
