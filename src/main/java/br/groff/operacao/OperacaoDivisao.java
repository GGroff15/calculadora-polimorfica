package br.groff.operacao;

class OperacaoDivisao implements IOperacao {

	@Override
	public int calcular(Valores valores) {
		return valores.dividir();
	}

}
