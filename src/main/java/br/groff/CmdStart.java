package br.groff;

import java.util.Scanner;

import br.groff.operacao.OperacaoFactory;
import br.groff.operacao.Valores;

public class CmdStart {

	private static Scanner scanner;

	public static void main(String[] args) {
		exibirCabecalho();

		scanner = new Scanner(System.in);
		// Entrada de dados usuario
		final var idOperacao = obterOperacaoSelecionadaPeloUsuario();
		final var valores = obterNumerosDigitadoPeloUsuario();

		final var operacao = OperacaoFactory.criar(idOperacao);
		final var resultado = operacao.calcular(valores);

		exibir(resultado);

		scanner.close();
	}

	private static void exibirCabecalho() {
		System.out.println("//***************************//");
		System.out.println(" Escolha uma operacao");
		System.out.println(" 1 - Adição");
		System.out.println(" 2 - Subtração");
		System.out.println(" 3 - Divisão");
		System.out.println(" 4 - Multiplicação");
		System.out.println(" 5 - Potenciação");
		System.out.println("//***************************//");
		System.out.println();
	}

	private static int obterOperacaoSelecionadaPeloUsuario() {
		System.out.print("Operação: ");
		return scanner.nextInt();
	}

	private static Valores obterNumerosDigitadoPeloUsuario() {
		System.out.print("Entre com o primeiro número: ");
		final var primeiroNumero = scanner.nextInt();
		System.out.print("Entre com o segundo número: ");
		final var segundoNumero = scanner.nextInt();
		return new Valores(primeiroNumero, segundoNumero);
	}

	private static void exibir(final int resultado) {
		System.out.println("Resultado: " + resultado);
	}

}
