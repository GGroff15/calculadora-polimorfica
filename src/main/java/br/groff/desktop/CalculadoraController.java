package br.groff.desktop;

import br.groff.operacao.IOperacao;
import br.groff.operacao.OperacaoFactory;
import br.groff.operacao.Valores;

public class CalculadoraController {

	private IOperacao operacao;

	/**
	 * @return the idOperacao
	 */
	public void setOperacao(int idOperacao) {
		operacao = OperacaoFactory.criar(idOperacao);
	}

	public int calcular(String conteudoPrimeiroNumero, String conteudoSegundoNumero) {

		final var primeiroNumero = Integer.parseInt(conteudoPrimeiroNumero);
		final var segundoNumero = Integer.parseInt(conteudoSegundoNumero);
		final var valores = new Valores(primeiroNumero, segundoNumero);

		return operacao.calcular(valores);
	}

}
