package br.groff.desktop;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class CalculadoraView {

	protected Shell shlCalculadoraPolimorfica;
	private Text primeiroNumero;
	private Text segundoNumero;
	private CalculadoraController controller;
	private Label resultado;
	private Combo combo;

	/**
	 * Open the window.
	 */
	public void open() {
		controller = new CalculadoraController();
		final var display = Display.getDefault();
		createContents();
		shlCalculadoraPolimorfica.open();
		shlCalculadoraPolimorfica.layout();
		while (!shlCalculadoraPolimorfica.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	void createContents() {
		shlCalculadoraPolimorfica = new Shell();
		shlCalculadoraPolimorfica.setSize(368, 105);
		shlCalculadoraPolimorfica.setText("Calculadora Polimorfica");

		primeiroNumero = new Text(shlCalculadoraPolimorfica, SWT.BORDER);
		primeiroNumero.setBounds(10, 10, 76, 21);

		segundoNumero = new Text(shlCalculadoraPolimorfica, SWT.BORDER);
		segundoNumero.setBounds(189, 10, 76, 21);

		resultado = new Label(shlCalculadoraPolimorfica, SWT.NONE);
		resultado.setBounds(271, 13, 55, 15);
		resultado.setText("New Label");

		combo = new Combo(shlCalculadoraPolimorfica, SWT.READ_ONLY);
		combo.setItems("", "Adição", "Subtração", "Divisao", "Multiplicacao", "Potenciação");
		combo.setBounds(92, 10, 91, 23);

		final var calcular = new Button(shlCalculadoraPolimorfica, SWT.NONE);
		calcular.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcular();
			}
		});
		calcular.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				calcular();
			}
		});
		calcular.setBounds(271, 34, 75, 25);
		calcular.setText("Calcular");

	}

	private void calcular() {
		controller.setOperacao(combo.getSelectionIndex());
		final var conteudoPrimeiroNumero = primeiroNumero.getText();
		final var conteudoSegundoNumero = segundoNumero.getText();

		final var resultadoCalculo = controller.calcular(conteudoPrimeiroNumero, conteudoSegundoNumero);
		resultado.setText(Integer.toString(resultadoCalculo));
	}
}
