package br.groff.operacao;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

class OperacaoSubtracaoTest {

	private final IOperacao operacao = new OperacaoSubtracao();

	@Test
	void testCalcular() {
		assertEquals("O resultado da subtracao de 3 - 2, deveria ser 1", 1, operacao.calcular(new Valores(3, 2)));
	}

}
