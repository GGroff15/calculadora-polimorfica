package br.groff.operacao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class OperacaoMultiplicacaoTest {

	@Test
	void testCalcular() {
		final IOperacao operacao = new OperacaoMultiplicacao();
		assertEquals(6, operacao.calcular(new Valores(2, 3)), "O valor retornado deveria ser 6");
	}

}
