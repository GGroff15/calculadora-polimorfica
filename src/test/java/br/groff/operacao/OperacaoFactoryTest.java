package br.groff.operacao;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class OperacaoFactoryTest {

	@Test
	void testCriarOperacaoAdicao() {
		final var operacao = OperacaoFactory.criar(IOperacao.ADICAO);
		assertTrue("O objeto retornado não é uma instancia da classe OperacaoAdicao",
				operacao instanceof OperacaoAdicao);
	}

	@Test
	void testCriarOperacaoSubtracao() {
		final var operacao = OperacaoFactory.criar(IOperacao.SUBTRACAO);
		assertTrue("O objeto retornado não é uma instancia da classe OperacaoSubtracao",
				operacao instanceof OperacaoSubtracao);
	}

	@Test
	void testCriarOperacaoDivisao() {
		final var operacao = OperacaoFactory.criar(IOperacao.DIVISAO);
		assertTrue("O objeto retornado não é uma instancia da classe OperacaoDivisao",
				operacao instanceof OperacaoDivisao);
	}

	@Test
	void testCriarOperacaoMultiplicacao() {
		final var operacao = OperacaoFactory.criar(IOperacao.MULTIPLICACAO);
		assertTrue("O objeto retornado não é uma instancia da classe OperacaoMultiplicacao",
				operacao instanceof OperacaoMultiplicacao);
	}

	@Test
	void testCriarOperacaoPotenciacao() {
		final var operacao = OperacaoFactory.criar(IOperacao.POTENCIACAO);
		assertTrue("O objeto retornado não é uma instancia da classe OperacaoPotenciacao",
				operacao instanceof OperacaoPotenciacao);
	}

	@Test
	void testCriarOperacaoInexistente() {
		assertThrows(IllegalArgumentException.class, () -> OperacaoFactory.criar(0),
				"Esperada uma excessao do tipo IllegalArgumentException");
	}

}
