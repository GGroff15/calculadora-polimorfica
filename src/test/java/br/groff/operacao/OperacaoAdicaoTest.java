package br.groff.operacao;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

class OperacaoAdicaoTest {

	private final IOperacao operacao = new OperacaoAdicao();

	@Test
	void testCalcular() {
		assertEquals("O resultado da soma 1 + 2, deveria ser 3", 3, operacao.calcular(new Valores(1, 2)));
	}

}
