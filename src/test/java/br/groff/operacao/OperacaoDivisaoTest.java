package br.groff.operacao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class OperacaoDivisaoTest {

	@Test
	void testCalcular() {
		final IOperacao operacao = new OperacaoDivisao();
		assertEquals(3, operacao.calcular(new Valores(6, 2)), "O resultado da divisao de 6 por 2 deveria ser 3");
	}

}
