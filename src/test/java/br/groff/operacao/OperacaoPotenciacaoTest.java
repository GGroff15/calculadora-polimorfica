package br.groff.operacao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class OperacaoPotenciacaoTest {

	@Test
	void testCalcular() {
		final IOperacao operacao = new OperacaoPotenciacao();
		assertEquals(8, operacao.calcular(new Valores(2, 3)), "O resultado de 2 elevado a 3 dedveria ser 8");
	}

}
